# Spring Boot Excel and CSV files Demo

This project presents a simple use of uploading, reading, writing and exporting Excel and CSV files. 
The relevant components of the project are: Java 8, Spring Boot, the Apache POI and the Apache Commons CSV dependencies.

The Apache POI dependency can handle both xls and xlsx files.

For easy testing, the projects uses Swagger UI.

## Testing

Launch the Spring Boot application and go to :

http://localhost:8080/swagger-ui.html

![Main Swagger Window](doc_images/swagger_main.png?raw=true "Main Swagger Window]")

As you can see, Swagger exposes six endpoints, with their description.

### Upload and read a CSV file's content

The first method, the POST _/read-external-csv-file_ allows you to upload a CSV file. It then reads the file data and it 
returns it within a Map. To test the method, click on _Try it out_, choose a CSV
file and _Execute_. If the file is not of type CSV, you will receive an error message.

![Uploaded file returned content](doc_images/uploaded_csv_file.png?raw=true "Uploaded file returned content]")


In a real life example you could use the data retrieved from the CSV file to persist it in a database.

### Upload and read a Excel file's content

The second method, the POST _/read-external-excel-file_ allows you to upload an Excel file, and it returns a JSON
containing the data of the first sheet/slide of the file. To test the method, click on _Try it out_, choose an Excel
file and _Execute_. If the file is not of type Excel (xls or xlsx), you will receive an error message.

![Uploaded file returned content](doc_images/uploaded_excel_file.png?raw=true "Uploaded file returned content]")

In a real life example you could use the data retrieved from the Excel file to persist it in a database.

### Read the default CSV file's content

The method GET _/excel/read-test-csv-file_ allows you to read the data  of a
default CSV file, stored at _example_files/users.csv_. 

You can modify the logic used by this endpoint and the method called from the CsvService to adapt it for reading the
properties of other objects that are defined as entities. This will allow you, for example, to read data from a CSV
file and save it within a database.

### Read the default Excel file's content

The method GET _/excel/read-test-excel-file_ allows you to read the data of the first sheet/slide of a
default Excel file, stored at _example_files/users.xlsx_.

### Write data to a CSV file and return it

The method GET _/excel/test-csv-export_ allows you to write data to a downloadable CSV file. 
For this example, fake User objects are created. 

In a real life example, you could use data retrieved from a database.

![Downloaded file](doc_images/download_csv_file.png?raw=true "Downloaded file]")

### Write data to an Excel file and return it

The method GET _/excel/test-excel-export_ allows you to write data to a downloadable Excel file.
For this example, fake User objects are created.

In a real life example, you could use data retrieved from a database.

## Configuration

The SwaggerConfig class contains the Swagger UI configuration.

The BeansConfig class files contains the declaration of the two Apache POI classes needed for reading and writing Excel
files :
XSSFWorkbook and XSSFSheet.

The pom.xml file contains the Swagger and the Apache POI dependencies, amongst others. Note also the presence of the '
javafaker' dependency, used for creating fake data.

IOException and IncompatibleFileTypeException are handled by the CustomExceptionHandler class.
