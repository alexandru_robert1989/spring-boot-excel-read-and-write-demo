package com.example.springbootexceldemo.services;

import com.example.springbootexceldemo.domain.User;

import java.util.List;

public interface UsersGeneratorService {

     List<User> createTestUsers(int totalUsers);

}
