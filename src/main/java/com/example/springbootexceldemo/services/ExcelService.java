package com.example.springbootexceldemo.services;

import com.example.springbootexceldemo.domain.User;
import com.example.springbootexceldemo.exception.IncompatibleFileTypeException;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface ExcelService {


    void writeHeaderLine(XSSFSheet sheet, XSSFWorkbook workbook, boolean isBold, int fontHeight, String[] columnNames);

    void createCell(XSSFSheet sheet, Row row, int columnCount, Object value, CellStyle style);

    void writeDateLines(XSSFSheet sheet, List<User> users, int fontHeight);

    void usersToExcel(int totalUsers, String fileName, boolean headerIsBold, int fontHeight, String[] columnNames, List<User> users, HttpServletResponse response) throws IOException;

    List<User> readTestUsersExcelFile() throws IOException;

    Map<Integer, List<String>> readExternalExcelFile(MultipartFile file) throws IOException;

    void isExcelFile(MultipartFile file) throws IncompatibleFileTypeException;



}
