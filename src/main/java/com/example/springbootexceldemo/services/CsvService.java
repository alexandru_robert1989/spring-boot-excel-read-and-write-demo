package com.example.springbootexceldemo.services;

import com.example.springbootexceldemo.domain.User;
import com.example.springbootexceldemo.exception.IncompatibleFileTypeException;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface CsvService {




    Map<Long, Map<String,String>> readExternalCsvFile(MultipartFile file) throws IOException;

    Map<Long, Map<String,String>> readTestUsersCsvFile() throws IOException;

    ByteArrayInputStream usersToCsv(List<User> users);

    void isCsvFile(MultipartFile file) throws IncompatibleFileTypeException;



}
