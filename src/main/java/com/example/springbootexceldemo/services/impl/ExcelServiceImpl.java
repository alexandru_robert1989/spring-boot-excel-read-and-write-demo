package com.example.springbootexceldemo.services.impl;

import com.example.springbootexceldemo.domain.User;
import com.example.springbootexceldemo.exception.IncompatibleFileTypeException;
import com.example.springbootexceldemo.services.ExcelService;
import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
@RequiredArgsConstructor
public class ExcelServiceImpl implements ExcelService {

    // test User(s) Excel file path
    String EXCEL_FILE_PATH = "example_files/users.xlsx";

    //used for the Excel file
    private final XSSFWorkbook workbook;

    //used for the Excel sheet/slide
    private final XSSFSheet sheet;


    /**
     * Write an Excel file's header values (column names).
     *
     * @param sheet       a {@link XSSFSheet}, representing a Excel file sheet/slide
     * @param workbook    a {@link XSSFWorkbook}, representing an Excel file
     * @param isBold      boolean value indicating if the font is bold
     * @param fontHeight  the font height
     * @param columnNames the Excel file column names/headers
     */
    @Override
    public void writeHeaderLine(XSSFSheet sheet, XSSFWorkbook workbook, boolean isBold, int fontHeight, String[] columnNames) {

        //create first row (containing the header values)
        Row row = sheet.createRow(0);

        //create style
        CellStyle style = workbook.createCellStyle();

        //create font than set its properties
        XSSFFont font = workbook.createFont();
        font.setBold(isBold);
        font.setFontHeight(fontHeight);

        //set style font
        style.setFont(font);

        // add column names (header values)
        for (int i = 0; i < columnNames.length; i++) {
            createCell(sheet, row, i, columnNames[i], style);
        }
    }


    /**
     * Method used for adding values to Excel files.
     *
     * @param sheet       a {@link XSSFSheet}, representing a Excel file sheet/slide
     * @param row         a {@link Row}, representing an Excel file row
     * @param columnCount an int value representing the column position
     * @param value       the value that will be written inside an Excel column
     * @param style       a {@link CellStyle}, used for setting the column style
     */
    @Override
    public void createCell(XSSFSheet sheet, Row row, int columnCount, Object value, CellStyle style) {

        sheet.autoSizeColumn(columnCount);

        //create cell at a given column position
        Cell cell = row.createCell(columnCount);

        // check data type
        if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        } else {
            cell.setCellValue((String) value);
        }

        cell.setCellStyle(style);
    }


    /**
     * Method adding an Excel file's data lines.
     *
     * @param sheet      a {@link XSSFSheet}, representing a Excel file sheet/slide
     * @param users      a     {@link List} of {@link User}s
     * @param fontHeight the font height
     */
    @Override
    public void writeDateLines(XSSFSheet sheet, List<User> users, int fontHeight) {

        // start from first row after the header
        int rowCount = 1;

        //create cell style
        CellStyle style = workbook.createCellStyle();

        //create font and set its properties
        XSSFFont font = workbook.createFont();
        font.setBold(false);
        font.setFontHeight(fontHeight);

        //set style font
        style.setFont(font);

        //fill columns with data
        for (User user : users) {

            //create row at next line
            Row row = sheet.createRow(rowCount++);

            //start writing from first column
            int columnCount = 0;

            //write data
            createCell(sheet, row, columnCount++, user.getFirstName(), style);
            createCell(sheet, row, columnCount++, user.getLastName(), style);
            createCell(sheet, row, columnCount++, user.getEmail(), style);
            createCell(sheet, row, columnCount++, user.getAge().toString(), style);
        }
    }

    /**
     * Method writing data to an Excel file that is returned within a {@link HttpServletResponse}.
     *
     * @param totalUsers   int value, representing the total number of {@link User} created using a Faker object
     * @param fileName     {@link String}  value, representing the root name of the exported file
     * @param headerIsBold boolean value indicating if the header font is bold
     * @param fontHeight   int value indicating the font height
     * @param columnNames  a {@link String} array representing the table's column names
     * @param users        a {@link List} of {@link User}s
     * @param response     a {@link HttpServletResponse},
     * @throws IOException {@link IOException}, thrown if writting the file fails
     */
    @Override
    public void usersToExcel(int totalUsers, String fileName, boolean headerIsBold,
                             int fontHeight, String[] columnNames, List<User> users, HttpServletResponse response)
            throws IOException {

        //actualise sheet default name, supplied with its Bean
        workbook.setSheetName(workbook.getSheetIndex(this.sheet), fileName);

        //fake header data, relevant for this example
        String[] testHeaderNames = {"First name", "Last name", "Email", "Age"};

        //create the header line
        writeHeaderLine(this.sheet, this.workbook, headerIsBold, fontHeight, testHeaderNames);

        //write data lines
        writeDateLines(this.sheet, users, fontHeight);

        //try with resources block, used for automatically closing the servletOutputStream
        try (ServletOutputStream servletOutputStream = response.getOutputStream()) {
            this.workbook.write(servletOutputStream);
        } catch (IOException e) {
            e.printStackTrace();
            // the IOException will be handled by the CustomExceptionHandler class
            throw e;
        }
    }


    /**
     * Method reading the content of an Excel file containing {@link User}s data.
     * Once the date was read, it is written within a List of {@link User}s that is then returned.
     * In a real life example, the data could be persisted in a database.
     *
     * @return a {@link List} of {@link User}s
     * @throws IOException {@link IOException}, thrown if retrieving the Excel file data fails
     */
    @Override
    public List<User> readTestUsersExcelFile() throws IOException {

        //data container
        List<User> users = new ArrayList<>();

        //can throw IOException
        Workbook workbook = WorkbookFactory.create(new FileInputStream(EXCEL_FILE_PATH));

        DataFormatter dataFormatter = new DataFormatter();
        CreationHelper creationHelper = workbook.getCreationHelper();
        FormulaEvaluator formulaEvaluator = creationHelper.createFormulaEvaluator();

        // get the Excel file's first sheet/slide
        Sheet sheet = workbook.getSheetAt(0);

        for (Row row : sheet) {
            // ignore the header values
            if (row.getRowNum() == 0) {
                continue;
            }

            //User properties
            String firstName = null;
            String lastName = null;
            String email = null;
            Integer age = null;

            for (Cell cell : row) {
                String cellContent = dataFormatter.formatCellValue(cell, formulaEvaluator);
                System.out.println("cell column index " + cell.getColumnIndex());

                // complete User properties according to the column position
                switch (cell.getColumnIndex()) {
                    case 0:
                        firstName = cellContent;
                        break;
                    case 1:
                        lastName = cellContent;
                        break;
                    case 2:
                        email = cellContent;
                        break;
                    case 3:
                        age = Integer.valueOf(cellContent);
                        break;
                    default:
                        break;
                }
            }

            users.add(User.builder()
                    .firstName(firstName)
                    .lastName(lastName)
                    .email(email)
                    .age(age)
                    .build());
        }

        workbook.close();

        return users;


    }


    /**
     * Method reading the content of an Excel file and returning it within a Map containing
     * each row's number and data.
     * In a real life example, the data could be persisted in a database.
     *
     * @param file a {@link MultipartFile}, that should be an Excel file
     * @return a Map<Integer, List<String>>
     * @throws IOException {@link IOException}, thrown if retrieving the Excel file data fails
     */
    @Override
    public Map<Integer, List<String>> readExternalExcelFile(MultipartFile file) throws IOException {

        //check file type
        isExcelFile(file);

        //data container
        Map<Integer, List<String>> tableData = new HashMap<>();

        //the Excel file
        XSSFWorkbook workbook = new XSSFWorkbook(file.getInputStream());

        //The Excel sheet/slide
        XSSFSheet worksheet = workbook.getSheetAt(0);


        DataFormatter dataFormatter = new DataFormatter();
        CreationHelper creationHelper = workbook.getCreationHelper();
        FormulaEvaluator formulaEvaluator = creationHelper.createFormulaEvaluator();

        //read Excel file data and write it within a Map
        for (Row row : worksheet) {
            List<String> columns = new ArrayList<>();
            int rowNumber = row.getRowNum();

            for (Cell cell : row) {

                String cellContent = dataFormatter.formatCellValue(cell, formulaEvaluator);
                columns.add(cellContent);
            }
            tableData.put(rowNumber, columns);
        }

        return tableData;
    }


    /**
     * Method checking if a file is of type Excel (xls or xlsx).
     *
     * @param file the tested {@link MultipartFile}
     * @throws IncompatibleFileTypeException exception thrown if  the file is not of type Excel
     */
    @Override
    public void isExcelFile(MultipartFile file) throws IncompatibleFileTypeException {

        //retrieve file extension
        String fileExtension = StringUtils.getFilenameExtension(file.getOriginalFilename());

        if (!org.apache.commons.lang3.StringUtils.equals(fileExtension, "xlsx") &&
                !org.apache.commons.lang3.StringUtils.equals(fileExtension, "xls")) {
            throw new IncompatibleFileTypeException("The file is not of type Excel");
        }
    }


}
