package com.example.springbootexceldemo.services.impl;

import com.example.springbootexceldemo.domain.User;
import com.example.springbootexceldemo.services.UsersGeneratorService;
import com.github.javafaker.Faker;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ThreadLocalRandom;

@Service
public class UsersGeneratorServiceImpl implements UsersGeneratorService {


    @Override
    public List<User> createTestUsers(int totalUsers) {
        List<User> users = new ArrayList<>();

        //used for generating fake data
        Faker faker = new Faker();

        List<String> firstNames = createFirstNames(faker, totalUsers);
        List<String> lastNames = createLastNames(faker, totalUsers);
        List<String> emails = createEmails(faker, totalUsers, firstNames, lastNames);


        for (int i = 0; i < totalUsers; i++) {
            users.add(User.builder()
                    .firstName(firstNames.get(i))
                    .lastName(lastNames.get(i))
                    .email(emails.get(i))
                    .age(ThreadLocalRandom.current().nextInt(18, 115))
                    .build());
        }

        return users;
    }


    private List<String> createFirstNames(Faker faker, int total) {
        List<String> firstNames = new ArrayList<>();

        for (int i = 0; i != total; i++) {
            firstNames.add(faker.name().firstName());
        }
        return firstNames;
    }


    private List<String> createLastNames(Faker faker, int total) {
        List<String> lastNames = new ArrayList<>();

        for (int i = 0; i != total; i++) {
            lastNames.add(faker.name().lastName());
        }
        return lastNames;
    }


    private List<String> createEmails(Faker faker, int total, List<String> firstNames, List<String> lastNames) {
        List<String> emails = new ArrayList<>();

        for (int i = 0; i != total; i++) {
            emails.add((firstNames.get(i) + "." + lastNames.get(i) + "@fmail.com").toLowerCase(Locale.ROOT));
        }
        return emails;
    }

}
