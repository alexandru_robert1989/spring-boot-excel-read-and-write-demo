package com.example.springbootexceldemo.services.impl;

import com.example.springbootexceldemo.domain.User;
import com.example.springbootexceldemo.exception.IncompatibleFileTypeException;
import com.example.springbootexceldemo.services.CsvService;
import org.apache.commons.csv.*;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class CsvServiceImpl implements CsvService {


    // test User(s) Excel file path
    String CSV_FILE_PATH = "example_files/users.csv";


    /**
     * Method reading and returning the data of an uploaded CSV file.
     *
     * @param file the CSV file, uploaded as a {@link MultipartFile}
     * @return a Map containing the data of the CSV file
     */
    @Override
    public Map<Long, Map<String, String>> readExternalCsvFile(MultipartFile file) {

        //check if csv file
        isCsvFile(file);

        //Map containing the returned data
        //in a real life example you could persist the data in a database
        Map<Long, Map<String, String>> tableData = new HashMap<>();

        //read the CSV file and write its content to a Map
        //using try with resources block to close the BufferedReader automatically
        try (BufferedReader fileReader = new BufferedReader(new InputStreamReader(file.getInputStream(), "UTF-8"));
             CSVParser csvParser = new CSVParser(fileReader,
                     CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());) {

            Iterable<CSVRecord> csvRecords = csvParser.getRecords();

            for (CSVRecord csvRecord : csvRecords) {
                //write CSW data
                Map<String, String> csvRecordValues = csvRecord.toMap();
                long index = csvRecord.getRecordNumber();
                tableData.put(index, csvRecordValues);
            }

            return tableData;
        } catch (IOException e) {
            throw new RuntimeException("fail to parse CSV file: " + e.getMessage());
        }
    }


    /**
     * Method reading the content of a CSV file containing {@link User}s data.
     * Once the date was read, it is written within a Map that is then returned.
     * In a real life example, the data could be persisted in a database.
     *
     * @return a Map containing the data of the CSV file
     * @throws IOException exception thrown if reading file bytes fails
     */
    @Override
    public Map<Long, Map<String, String>> readTestUsersCsvFile() throws IOException {

        File file = new File(CSV_FILE_PATH);
        MultipartFile result = new MockMultipartFile("file.csv",
                "file.csv", "csv", Files.readAllBytes(file.toPath()));

        return readExternalCsvFile(result);
    }


    /**
     * Method writing data to a CSV file and returning the file data as a {@link ByteArrayInputStream}.
     *
     * @param users a {@link List} of {@link User}s
     * @return a {@link ByteArrayInputStream}
     */
    @Override
    public ByteArrayInputStream usersToCsv(List<User> users) {

        CSVFormat format = CSVFormat.DEFAULT.withQuoteMode(QuoteMode.MINIMAL);

        try (ByteArrayOutputStream out = new ByteArrayOutputStream();
             CSVPrinter csvPrinter = new CSVPrinter(new PrintWriter(out), format.withHeader("First name", "Last name", "Email", "Age"));) {

            for (User user : users) {
                List<String> data = Arrays.asList(
                        String.valueOf(user.getFirstName()),
                        user.getLastName(),
                        user.getEmail(),
                        String.valueOf(user.getAge())
                );

                csvPrinter.printRecord(data);
            }

            csvPrinter.flush();
            return new ByteArrayInputStream(out.toByteArray());
        } catch (IOException e) {
            throw new RuntimeException("fail to import data to CSV file: " + e.getMessage());
        }
    }


    /**
     * Method checking if a file is of type CSV.
     *
     * @param file the tested {@link MultipartFile}
     * @throws IncompatibleFileTypeException exception thrown if  the file is not of type CSV
     */
    @Override
    public void isCsvFile(MultipartFile file) throws IncompatibleFileTypeException {

        //retrieve file extension
        String fileExtension = StringUtils.getFilenameExtension(file.getOriginalFilename());

        if (!org.apache.commons.lang3.StringUtils.equals(fileExtension, "csv")) {
            throw new IncompatibleFileTypeException("The file is not of type CSV");
        }
    }
}
