package com.example.springbootexceldemo.config;

import com.example.springbootexceldemo.exception.IncompatibleFileTypeException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.io.IOException;

@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {


    @ExceptionHandler(value = {IncompatibleFileTypeException.class})
    protected ResponseEntity<?> handleIncompatibleFileTypeException(IncompatibleFileTypeException ex, WebRequest request) {
        return ResponseEntity.badRequest().body(ex.getMessage());
    }

    @ExceptionHandler(value = {IOException.class})
    protected ResponseEntity<?> handleIOException(IOException ex, WebRequest request) {
        return ResponseEntity.badRequest().body(ex.getMessage());
    }


}
