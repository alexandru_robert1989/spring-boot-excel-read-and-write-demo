package com.example.springbootexceldemo.config;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeansConfig {

    @Bean
    public XSSFWorkbook workbook() {
        return new XSSFWorkbook();
    }


    @Bean
    public XSSFSheet sheet() {
        return workbook().createSheet("default sheet name");
    }


}
