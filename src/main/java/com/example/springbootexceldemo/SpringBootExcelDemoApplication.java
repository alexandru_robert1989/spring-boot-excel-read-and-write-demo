package com.example.springbootexceldemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootExcelDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootExcelDemoApplication.class, args);
    }

}
