package com.example.springbootexceldemo.exception;

public class IncompatibleFileTypeException extends RuntimeException{



    public IncompatibleFileTypeException(String message) {
        super(message);
    }
}
