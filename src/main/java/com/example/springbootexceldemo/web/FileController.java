package com.example.springbootexceldemo.web;


import com.example.springbootexceldemo.domain.User;
import com.example.springbootexceldemo.services.CsvService;
import com.example.springbootexceldemo.services.ExcelService;
import com.example.springbootexceldemo.services.UsersGeneratorService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

@RestController
@Controller
@RequestMapping("files")
@RequiredArgsConstructor
@Api("Controller exposing endpoints allowing an user to handle Excel and CSV files")
public class FileController {


    private final ExcelService excelService;
    private final CsvService csvService;
    private final UsersGeneratorService usersGeneratorService;



    @GetMapping("/test-excel-export")
    @ApiOperation("Generate and export a test Excel file containing Users")
    public void testExcelExport(HttpServletResponse response) throws IOException {

        // set the response properties
        response.setContentType("application/octet-stream");

        //create Date String value
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());

        //add header values
        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=users_" + currentDateTime + ".xlsx";
        response.setHeader(headerKey, headerValue);

        //create test Users
        int totalUsers = ThreadLocalRandom.current().nextInt(30, 100);
        List<User> testUsers = usersGeneratorService.createTestUsers(totalUsers);
        String[] headerNames = {"First name", "Last name", "Email", "Age"};

        //create and export file
        excelService.usersToExcel(totalUsers, "Users", true, 17, headerNames, testUsers, response);

    }


    @GetMapping("/test-csv-export")
    @ApiOperation("Generate and export a test CSV file containing Users")
    public ResponseEntity<Resource> testCsvExport() {

        //create Date String value
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());
        String filename = "users_" + currentDateTime + ".csv";

        //create test Users
        int totalUsers = ThreadLocalRandom.current().nextInt(30, 100);
        List<User>users = usersGeneratorService.createTestUsers(totalUsers);

        ByteArrayInputStream in = csvService.usersToCsv(users);

        InputStreamResource file = new InputStreamResource(in);

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename)
                .contentType(MediaType.parseMediaType("application/csv"))
                .body(file);
    }


    @ApiOperation("Read and return the content of the test Users Excel file")
    @GetMapping("/read-test-excel-file")
    public ResponseEntity<?> readTestExcelFile() throws IOException {
        return ResponseEntity.ok(excelService.readTestUsersExcelFile());
    }

    @ApiOperation("Read and return the content of the test Users CSV file")
    @GetMapping("/read-test-csv-file")
    public ResponseEntity<?> readTestCsvFile() throws IOException {
        return ResponseEntity.ok(csvService.readTestUsersCsvFile());
    }


    @ApiOperation("Upload an Excel file, read and return its content")
    @PostMapping(value = "/read-external-excel-file")
    public ResponseEntity<?> readExternalExcelFile(@RequestParam("file") MultipartFile file) throws IOException {
            return ResponseEntity.ok(excelService.readExternalExcelFile(file));
    }



    @ApiOperation("Upload an CSV file, read and return its content")
    @PostMapping(value = "/read-external-csv-file")
    public ResponseEntity<?> readExternalCsvFile(@RequestParam("file") MultipartFile file) throws IOException {
        return ResponseEntity.ok(csvService.readExternalCsvFile(file));
    }


}
